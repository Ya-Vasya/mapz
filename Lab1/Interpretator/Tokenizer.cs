﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Interpretator.Expressions;
using Interpretator.Expressions.LogicalExpressions;

namespace Interpretator
{
    public class Tokenizer
    {
        public Tokenizer() {}

        public List<string> tokenize(string inputString)
        {
            List<string> tokens = new List<string>();
            tokens = Regex.Split(inputString, @"((!=)*(==)*[*+/\-=; )(])|([0-9]+)").ToList();
            tokens.RemoveAll(x => x == " " || x == "" || x == "\r");
            for(int i = 0, n = tokens.Count; i < n; i++)
            {
                tokens[i] = tokens[i].Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
            }
            return tokens;
        }

        public void Analyze(ref List<List<Token>> tokenList, List<string> tokens)
        {
            int temp;
            tokenList.Add(new List<Token>());
            for (int i = 0, n = tokens.Count; i < n; i++)
            {
                Token tToken = new Token();
                if (tokens[i] == "var")
                {
                    tToken.Priority = 0;
                    tToken.ExprType = new VarExpression();
                    tokenList[tokenList.Count -1].Add(tToken);
                }
                else if (tokens[i] == "MoveMouse")
                {
                    tToken.Priority = 0;
                    tToken.ExprType = new MoveMouse();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if(tokens[i] == "LeftClick")
                {
                    tToken.Priority = 0;
                    tToken.ExprType = new LeftClick();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "RightClick")
                {
                    tToken.Priority = 0;
                    tToken.ExprType = new RightClick();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "DbLeftClick")
                {
                    tToken.Priority = 0;
                    tToken.ExprType = new DbLeftClick();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "MoveIcon")
                {
                    tToken.Priority = 0;
                    tToken.ExprType = new MoveIcon();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "OpExpl")
                {
                    tToken.Priority = 0;
                    tToken.ExprType = new OpenExplorerExpression();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "OpRunLine")
                {
                    tToken.Priority = 0;
                    tToken.ExprType = new WinRExpression();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "OpenExplorer")
                {
                    tToken.Priority = 0;
                    tToken.ExprType = new OpenExplorerExpression();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "OpenBrowser")
                {
                    tToken.Priority = 0;
                    tToken.ExprType = new OpenBrowser();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "OpenCmd")
                {
                    tToken.Priority = 0;
                    tToken.ExprType = new cmdExpression();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if(tokens[i] == "if")
                {
                    tToken.Priority = 7;
                    tToken.ExprType = new IfExpression();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if(tokens[i] == "else")
                {
                    tToken.Priority = 7;
                    tToken.ExprType = new ElseExpression();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if(tokens[i] == "while")
                {
                    tToken.Priority = 7;
                    tToken.ExprType = new WhileExpression();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "+")
                {
                    tToken.Priority = 6;
                    tToken.ExprType = new PlusExpression();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "*")
                {
                    tToken.Priority = 7;
                    tToken.ExprType = new MultiplyExpression();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "/")
                {
                    tToken.Priority = 7;
                    tToken.ExprType = new DivisionExpression();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "-")
                {
                    tToken.Priority = 6;
                    tToken.ExprType = new MinusExpression();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "(")
                {
                    tToken.Priority = 9;
                    tToken.ExprType = new LBracketExpression();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == ")")
                {
                    tToken.Priority = 9;
                    tToken.ExprType = new RBracketExpression();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "{")
                {
                    tToken.Priority = 0;
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "}")
                {
                    tToken.Priority = 0;
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "==")
                {
                    tToken.Priority = 4;
                    tToken.ExprType = new Equals();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "!=")
                {
                    tToken.Priority = 4;
                    tToken.ExprType = new NotEqual();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == ">")
                {
                    tToken.Priority = 5;
                    tToken.ExprType = new Greater();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "<")
                {
                    tToken.Priority = 5;
                    tToken.ExprType = new Less();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "&&")
                {
                    tToken.Priority = 3;
                    tToken.ExprType = new And();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "||")
                {
                    tToken.Priority = 2;
                    tToken.ExprType = new OrExp();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (tokens[i] == "=")
                {
                    tToken.Priority = 1;
                    tToken.ExprType = new AssignmentExpression();
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (int.TryParse(tokens[i], out temp) == true)
                {
                    tToken.ExprType = new NumberExpression(temp);
                    tToken.Priority = 8;
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
                else if (Regex.IsMatch(tokens[i], @"\w+"))
                {
                    tToken.ExprType = new VariableExpression(tokens[i]);
                    tToken.Priority = 8;
                    tokenList[tokenList.Count - 1].Add(tToken);
                }
            }
        }
    }
}