﻿namespace Interpretator.Expressions
{
    abstract class ConditionalExpression
    {
        public IExpression condition { get; set; }
        public IExpression positiveBlock { get; set; }
        
        protected ConditionalExpression()
        {
            condition = null;
            positiveBlock = null;
        }
    }
}
