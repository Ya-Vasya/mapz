﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interpretator.Expressions
{
    public class NumberExpression:IExpression
    {
        private double value;

        public NumberExpression(double value)
        {
            this.value = value;
        }

        public object Eval(Context context)
        {
            return Convert.ToDouble(value);
        }
    }
}
