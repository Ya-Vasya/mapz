﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interpretator.Expressions
{
    public abstract class UnaryExpression : IExpression
    {
        public IExpression expr1;

        public abstract object Eval(Context context);
    }
}
