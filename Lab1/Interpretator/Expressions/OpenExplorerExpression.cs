﻿namespace Interpretator.Expressions
{
    class OpenExplorerExpression : IExpression
    {
        public object Eval(Context context)
        {
            KeyEvents.keybd_event((byte)0x5B, 0, 0x0001 | 0, 0);
            KeyEvents.keybd_event((byte)0x45, 0, 0x0001 | 0, 0);
            KeyEvents.keybd_event((byte)0x45, 0, 0x0001 | 0x0002, 0);
            KeyEvents.keybd_event((byte)0x5B, 0, 0x0001 | 0x0002, 0);
            return "";
        }
    }
}