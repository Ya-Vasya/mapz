﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpretator.Expressions.LogicalExpressions
{
    class And : BinaryExpression
    {
        public override object Eval(Context context)
        {
            int result = 0;
            object first = expr1.Eval(context);
            object second = expr2.Eval(context);
            if(expr1 != null)
                if(first is double fr)
                    if (fr > 0)
                    {
                        if (second != null)
                            if (second is double sr)
                                if (sr > 0)
                                    result = 1;
                    }
            return result;
        }
    }
}
