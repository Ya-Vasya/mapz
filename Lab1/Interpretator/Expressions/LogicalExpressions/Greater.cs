﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpretator.Expressions.LogicalExpressions
{
    class Greater : BinaryExpression
    {
        public override object Eval(Context context)
        {
            object first = expr1.Eval(context);
            object second = expr2.Eval(context);
            if (first is double f && second is double s)
                return (f > s) ? 1 : 0;
            //todo throw exception
            return 0;
        }
    }
}
