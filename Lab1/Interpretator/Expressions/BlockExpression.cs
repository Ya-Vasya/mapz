﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpretator.Expressions
{
    class BlockExpression : IExpression
    {
        List<IExpression> expressionList;

        public BlockExpression()
        {
            expressionList = new List<IExpression>();
        }

        public void addToList(IExpression expr)
        {
            expressionList.Add(expr);
        }

        public object Eval(Context context)
        {
            object result = null;
            foreach (IExpression i in expressionList)
            {
                result = i.Eval(context);
            }
            return result;
        }
    }
}
