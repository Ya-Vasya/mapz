﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interpretator.Expressions
{
    public class VariableExpression : IExpression
    {
        public string name { get; private set; }

        public VariableExpression(string value)
        {
            this.name = value;
        }

        public object Eval(Context context)
        {
            return Convert.ToDouble(context.GetVariable(name));
        }
    }
}
