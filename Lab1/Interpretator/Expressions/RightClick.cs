﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpretator.Expressions
{
    class RightClick : IExpression
    {

        public object Eval(Context context)
        {
            MouseEvents.MouseEvent(MouseEvents.MouseEventFlags.RightDown);
            MouseEvents.MouseEvent(MouseEvents.MouseEventFlags.RightUp);
            return " " + '\b';
        }
    }
}
