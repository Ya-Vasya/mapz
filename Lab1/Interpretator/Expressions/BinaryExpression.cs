﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interpretator.Expressions
{
    public abstract class  BinaryExpression:IExpression
    {
        public IExpression expr1;
        public IExpression expr2;

        abstract public object Eval(Context context);
    }
}