﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game001
{
    public class GameState : State
    {

        #region Fields
        CCharachter mainPlayer;
        CollisionManager collisionManager;
        Texture2D npcTexture;
        List<IItem> items;
        Texture2D notebookTexture;
        Vector2 notebookPos;
        CharachterFactory charachterFactory;
        SpriteFont _font;
        MyGameTime myGameTime;
        Vector2 TimePos;
        string charName = "Mark";
        Color color = Color.Black;
        SaveManager saveManager;
        InteractionManager interactionManager;
        Command _command;
        public GameHistory History = new GameHistory();
        ServerManager serverManager;
        string _recievedMessage;
        #endregion

        #region Methods

        public GameState(Game1 game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
        {
            Initialize();
            LoadContent();
            AddButtons();
            History.LastState = mainPlayer.SaveState();
            saveManager = new SaveManager(History.LastState);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.DrawString(_font, _recievedMessage, new Vector2(10, 30), Color.White);
            mainPlayer.Draw(spriteBatch);
            spriteBatch.DrawString(_font, myGameTime.GetCurrentTime(), TimePos, Color.White);
            foreach (IItem item in items)
            {
                item.Draw(spriteBatch, _font);
            }
            DrawComponents(gameTime, spriteBatch);
            charachterFactory.Draw(spriteBatch, _font);

            spriteBatch.End();
        }

        public override void PostUpdate(GameTime gameTime)
        {
            
        }

        public override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                _game.Exit();

            collisionManager.Update();
            mainPlayer.Update();
            myGameTime.Update();
            interactionManager.Update();
            foreach (IItem item in items)
            {
                item.Update();
            }
            UpdateComponents(gameTime);
            for (int i = 0; i < charachterFactory.getCharList().Count; i++)
            {
                charachterFactory.getCharList()[i].Update();
            }
        }

        public void Initialize()
        {
            myGameTime = MyGameTime.GetInstance();
            notebookPos = new Vector2(Game1.random.Next(0, 1920), Game1.random.Next(0, 1080));
            collisionManager = new CollisionManager();
            TimePos = new Vector2(Game1.width - 110, 30f);
            items = new List<IItem>();
            serverManager = new ServerManager();
            serverManager.SendMessage();
            _recievedMessage = serverManager.RecieveMessage();
        }

        public void LoadContent()
        {
            mainPlayer = CCharachter.getInstance(_content.Load<Texture2D>("PlayerModel"));
            npcTexture = _content.Load<Texture2D>("npc");
            notebookTexture = _content.Load<Texture2D>("Book");
            charachterFactory = new CharachterFactory(npcTexture);
            items.Add(new EmptyNotebook("sfe", notebookTexture, notebookPos, charName));
            _font = _content.Load<SpriteFont>("TimeFont");
            charachterFactory.createClassMate("Ross");
            items.Add(new DecentNotebook("Great work _teacher", notebookTexture, new Vector2(Game1.random.Next(0, 1880), Game1.random.Next(0, 1000)), charName));
            collisionManager.Main = mainPlayer;
            collisionManager.Charachters = charachterFactory.getCharList();
            collisionManager.ItemsList = items;
            _command = new Command();
            interactionManager = new InteractionManager(mainPlayer, charachterFactory.getCharList(), _command);
        }
        
        public void AddButtons()
        {
            Texture2D buttonTexture = _content.Load<Texture2D>("button");
            SpriteFont buttonFont = _content.Load<SpriteFont>("TimeFont");

            Button saveButton = new Button(buttonTexture, buttonFont)
            {
                Posiion = new Vector2(Game1.width - buttonTexture.Width - 150, 25),
                Text = "Save"
            };

            saveButton.Click += saveButton_Click;

            Button restoreButton = new Button(buttonTexture, buttonFont)
            {
                Posiion = new Vector2(Game1.width - buttonTexture.Width - 250, 25),
                Text = "Restore"
            };

            restoreButton.Click += restoreButton_Click;


            Button saveGameButton = new Button(buttonTexture, buttonFont)
            {
                Posiion = new Vector2(Game1.width - buttonTexture.Width - 350, 25),
                Text = "Save Game"
            };

            saveGameButton.Click += saveGameButton_Click;

            _components = new List<Component>()
            {
                saveButton,
                restoreButton,
                saveGameButton
            };
        }
        #endregion

        #region Events
        private void saveGameButton_Click(object sender, EventArgs e)
        {
            saveManager.Save(mainPlayer.SaveState());
        }

        private void restoreButton_Click(object sender, EventArgs e)
        {
            mainPlayer.RestoreState(History.LastState);
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            History.LastState = mainPlayer.SaveState();
        }
        #endregion

    }
}
