﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Game001
{
    public class MenuState : State
    {
        #region Methods

        public MenuState(Game1 game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
        {
            Texture2D buttonTexture = _content.Load<Texture2D>("MenuButton");
            SpriteFont buttonFont = _content.Load<SpriteFont>("TimeFont");

            Button newGameButton = new Button(buttonTexture, buttonFont)
            {
                Posiion = new Vector2(Game1.width / 2 - buttonTexture.Width / 2, Game1.height / 2 - 150),
                Text = "New Game"
            };

            newGameButton.Click += NewGameButton_Click;

            Button loadGameButton = new Button(buttonTexture, buttonFont)
            {
                Posiion = new Vector2(Game1.width / 2 - buttonTexture.Width / 2, Game1.height / 2),
                Text = "Load Game"
            };

            loadGameButton.Click += LoadGameButton_Click;

            Button quitGameButton = new Button(buttonTexture, buttonFont)
            {
                Posiion = new Vector2(Game1.width / 2 - buttonTexture.Width / 2, Game1.height / 2 + 150),
                Text = "Quit Game"
            };

            quitGameButton.Click += QuitGameButton_Click;

            _components = new List<Component>()
            {
                newGameButton,
                loadGameButton,
                quitGameButton
            };
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();

            DrawComponents(gameTime, spriteBatch);

            spriteBatch.End();
        }

        public override void PostUpdate(GameTime gameTime)
        {

        }

        public override void Update(GameTime gameTime)
        {
            UpdateComponents(gameTime);
        }
        #endregion

        #region Events
        private void QuitGameButton_Click(object sender, EventArgs e)
        {
            _game.Exit();
        }

        private void LoadGameButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new LoadedGame(_game, _graphicsDevice, _content));
        }

        private void NewGameButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new GameState(_game, _graphicsDevice, _content));
        }
        #endregion

    }
}
