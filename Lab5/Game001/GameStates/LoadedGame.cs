﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    public class LoadedGame: State
    {

            CCharachter mainPlayer;
            CollisionManager collisionManager;
            SpriteFont _font;
            MyGameTime myGameTime;
            Vector2 TimePos;
            Color color = Color.Black;
            SaveManager saveManager;
            public GameHistory History = new GameHistory();

            public LoadedGame(Game1 game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
            {
                Initialize();
                LoadContent();
                AddButtons();
                History.LastState = mainPlayer.SaveState();
                saveManager = new SaveManager(History.LastState);
        }

            public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
            {
                spriteBatch.Begin();

                mainPlayer.Draw(spriteBatch);
                spriteBatch.DrawString(_font, myGameTime.GetCurrentTime(), TimePos, Color.White);
                DrawComponents(gameTime, spriteBatch);

                spriteBatch.End();
            }

            public override void PostUpdate(GameTime gameTime)
            {

            }

            public override void Update(GameTime gameTime)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                    _game.Exit();

                collisionManager.Update();
                mainPlayer.Update();
                myGameTime.Update();
                UpdateComponents(gameTime);
            }

            public void Initialize()
            {
                myGameTime = MyGameTime.GetInstance();
                collisionManager = new CollisionManager();
                TimePos = new Vector2(Game1.width - 110, 30f);
            }

            public void LoadContent()
            {
                mainPlayer = CCharachter.getInstance(_content.Load<Texture2D>("PlayerModel"));
                saveManager.LoadGame();
                _font = _content.Load<SpriteFont>("TimeFont");
                collisionManager.Main = mainPlayer;
                /*collisionManager.Charachters = charachterFactory.getCharList();
                collisionManager.ItemsList = items;*/
            }

            public void AddButtons()
            {
                Texture2D buttonTexture = _content.Load<Texture2D>("button");
                SpriteFont buttonFont = _content.Load<SpriteFont>("TimeFont");

                Button saveButton = new Button(buttonTexture, buttonFont)
                {
                    Posiion = new Vector2(Game1.width - buttonTexture.Width - 150, 25),
                    Text = "Save"
                };

                saveButton.Click += saveButton_Click;

                Button restoreButton = new Button(buttonTexture, buttonFont)
                {
                    Posiion = new Vector2(Game1.width - buttonTexture.Width - 250, 25),
                    Text = "Restore"
                };

                restoreButton.Click += restoreButton_Click;


                Button saveGameButton = new Button(buttonTexture, buttonFont)
                {
                    Posiion = new Vector2(Game1.width - buttonTexture.Width - 350, 25),
                    Text = "Save Game"
                };

                saveGameButton.Click += saveGameButton_Click;

                _components = new List<Component>()
            {
                saveButton,
                restoreButton,
                saveGameButton
            };
            }

            private void saveGameButton_Click(object sender, EventArgs e)
            {
                saveManager.Save(mainPlayer.SaveState());
            }

            private void restoreButton_Click(object sender, EventArgs e)
            {
                mainPlayer.RestoreState(History.LastState);
            }

            private void saveButton_Click(object sender, EventArgs e)
            {
                History.LastState = mainPlayer.SaveState();
            }
    }
}
