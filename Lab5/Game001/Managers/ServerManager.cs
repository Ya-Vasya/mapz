﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    class ServerManager
    {

        #region Fields

        static int port = 8005;
        static string address = "127.0.0.1";
        Socket socket;

        #endregion

        #region Methods

        public ServerManager()
        {

        }

        public void SendMessage()
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(address), port);

            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(ipPoint);
            string message = "Test Message! Only for demonstration purposes";
            byte[] data = Encoding.Unicode.GetBytes(message);
            socket.Send(data);
        }

        public string RecieveMessage()
        {
            byte[] data = new byte[256]; // буфер для ответа
            StringBuilder builder = new StringBuilder();
            int bytes = 0; // количество полученных байт

            do
            {
                bytes = socket.Receive(data, data.Length, 0);
                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
            }
            while (socket.Available > 0);

            socket.Shutdown(SocketShutdown.Both);
            socket.Close();

            return builder.ToString();
        }

        #endregion

    }
}
