﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    public class Command //Command
    {

        public CBasicCharachter interactableCharachter;

        #region Methods

        public Command()
        {
            interactableCharachter = null;
        }

        public Command(CBasicCharachter charachter)
        {
            interactableCharachter = charachter;
        }

        public void SetToNull()
        {
            interactableCharachter = null;
        }

        public void setNewCharachter(CBasicCharachter charachter)
        {
            interactableCharachter = charachter;
        }

        public void Execute() => interactableCharachter.ChangePosition();

        #endregion


    }
}
