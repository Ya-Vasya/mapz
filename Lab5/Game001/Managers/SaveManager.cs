﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Game001
{
    public class SaveManager
    {

        private static string path = "lastSave.xml";
        HeroMemento hero;

        public SaveManager(HeroMemento charachter)
        {
            hero = charachter;
        }

        private static void SaveGame(HeroMemento charachter)
        {
            IFormatter bf = new BinaryFormatter();
            Stream stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None);
            bf.Serialize(stream, charachter);
            stream.Close();
        }

        public void Save(HeroMemento charachter)
        {
            SaveGame(charachter);
        }

        public void LoadGame()
        {
            IFormatter bf = new BinaryFormatter();
            Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            hero = (HeroMemento)bf.Deserialize(stream);
            stream.Close();
        }

    }
}
