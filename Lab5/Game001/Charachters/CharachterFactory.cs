﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    class CharachterFactory
    {
        #region Fields
        private Dictionary<string, CBasicCharachter> _charachters = new Dictionary<string, CBasicCharachter>();
        private Texture2D _classMate, _teacher;
        #endregion

        #region Main Methods
        public CharachterFactory(Texture2D classmate /*Texture2D _teacher*/)
        {
            _classMate = classmate;
            //this._teacher = _teacher;
            _charachters.Add("Bob", new ClassMate(_classMate, "Carl", Game1.random));
        }

        public void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            foreach (var ch in _charachters.Keys)
            {
                if (_charachters[ch] is ClassMate)
                {
                    spriteBatch.Draw(_classMate, _charachters[ch].Position, Color.White);
                    _charachters[ch].Draw(spriteBatch, font);
                }
            }
        }
        #endregion

        #region Additional Methods
        public CBasicCharachter createClassMate(string name)
        {
            _charachters.Add(name, new ClassMate(_classMate, name, Game1.random));
            return _charachters[name];
        }

        /*
        public CBasicCharachter createTeacher(string name, Subject hisClass)
        {
            charachters.Add(name, new Teacher(teachers, name, hisClass, rand));
            return charachters[name];
        }
        */

        public CBasicCharachter GetCharachter(string key)
        {
            if (_charachters.ContainsKey(key))
                return _charachters[key];
            else
                return null;
        }

        public List<CBasicCharachter> getCharList()
        {
            return _charachters.Values.ToList();
        }
        #endregion

    }
}
