﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    public abstract class CBasicCharachter
    {
        #region Fields
        protected Texture2D _texture;
        private Vector2 _position;
        private Rectangle _bounds;
        private string _heroPosition;
        #endregion

        #region Properties
        public string HeroPosition { get { return _heroPosition; } set { _heroPosition = value; } }
        public string Name { get; private set; }
        public Vector2 Position { get { return _position; } set { _position = value; } }
        public Rectangle Bounds { get { return _bounds; } private set { _bounds = value; } }
        #endregion

        #region Main Methods
        public CBasicCharachter(Texture2D texture, string name, Random rand)
        {
            _texture = texture;
            setRandomPos(rand);
            Bounds = new Rectangle((int)_position.X, (int)_position.Y, _texture.Width, _texture.Height);
            this.Name = name;
        }

        public void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            spriteBatch.Draw(_texture, _position, null, Color.White);
            spriteBatch.DrawString(font, HeroPosition, new Vector2(_position.X, _position.Y - 30), Color.White);
        }

        virtual public void Update()
        {

        }
        #endregion

        //Command, Observer
        #region Additional Methods

        //For pattern Command
        public void ChangePosition()
        {
            _position.X += 5;
            _position.Y += 5;
            _bounds.X += 5;
            _bounds.Y += 5;
        }

        //For spawning
        public void setRandomPos(Random rand)
        { 
            _position = new Vector2(rand.Next(0, 1920), rand.Next(0, 1080));
        }

        //For pattern Observer
        public void UpdateHeroPosition(Vector2 pos)
        {
            _heroPosition = $"Hero: {pos.X}, {pos.Y}";
        }
        #endregion
    }
}
