﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Game001
{
    public class MyGameTime //Singleton
    {

        #region Fields
        private static readonly Lazy<MyGameTime> instance = new Lazy<MyGameTime>(() => new MyGameTime());
        public string Date { get; private set; }
        #endregion

        #region Methods
        private MyGameTime()
        {
            Date = DateTime.Now.ToString("HH:mm:ss");
        }

        public string GetCurrentTime()
        {
            return Date;
        }
        
        public void Update()
        {
            Date = DateTime.Now.ToString("HH:mm:ss");
        }

        public static MyGameTime GetInstance()
        {
            return instance.Value;
        }
        #endregion

    }
}
