﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    public abstract class BaseNotebook : IItem
    {

        private string _innerText;
        public string InnerText { get { return _innerText; } set { _innerText = value; } }
        private string _belongsTo;
        public string BelongsTo { get { return _belongsTo; } set { _belongsTo = value; } }
        private Texture2D _texture;
        public Texture2D Texture { get { return _texture; } set { _texture = value; } }
        public Vector2 screenPosition;
        private bool _isRemoved = false;
        public bool IsRemoved { get { return _isRemoved; } set { IsRemoved = value; } }
        public Rectangle Bounds { get { return _bounds; } private set { _bounds = value; } }
        private Rectangle _bounds;



        public BaseNotebook(string Text, Texture2D texture, Vector2 screenPos, string belongs)
        {
            InnerText = Text;
            Texture = texture;
            screenPosition = screenPos;
            BelongsTo = belongs;
            Bounds = new Rectangle((int)screenPosition.X, (int)screenPosition.Y, _texture.Width, _texture.Height);
        }

        public abstract BaseNotebook Clone();

        public string readText()
        {
            return InnerText;
        }

        public bool Intersects(CCharachter mainPlayer)
        {
            if(Bounds.Intersects(mainPlayer.Bounds))
            {
                return true;
            }
            return false;
        }

        public virtual void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            spriteBatch.Draw(_texture, screenPosition, null, Color.White);
            spriteBatch.DrawString(font, "Notebook", new Vector2(screenPosition.X, screenPosition.Y - 70), Color.White);
        }

        public void Update()
        {
            
        }

        public void setRandomPos()
        {
            screenPosition = new Vector2(Game1.random.Next(0, 1900), Game1.random.Next(0, 1000));
        }

    } 
}
