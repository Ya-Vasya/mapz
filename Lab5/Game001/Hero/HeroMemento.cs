﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    [Serializable]
    public class HeroMemento //Memento
    {

        private Vector2 _position;
        private Rectangle _bounds;

        public Vector2 Position { get { return _position; } set { _position = value; } }
        public Rectangle Bounds { get { return _bounds; } private set { _bounds = value; } }

        public HeroMemento(Vector2 position, Rectangle bounds)
        {
            Position = position;
            Bounds = bounds;
        }

    }
}
