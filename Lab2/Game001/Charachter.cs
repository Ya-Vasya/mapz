﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    class CCharachter
    {
        
        private static CCharachter instance;
        private Vector2 Origin;

        public Texture2D _texture;
        public Vector2 Position { get { return _position; } set { _position = value; } }
        private Vector2 _position;
        public Rectangle Bounds { get { return _bounds; } private set { _bounds = value; } }
        private Rectangle _bounds;
        private float _rotation = 0;
        public float _speed = 5f;
        public Vector2 Velocity;
        public string Name { get; private set; }
        public Vector2 PrevPos { get { return _prevPos; } set { _prevPos = value; } }
        private Vector2 _prevPos;

        private static object syncRoot = new Object();

        private CCharachter(Texture2D texture)
        {
            _texture = texture;
            Position = new Vector2(GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width / 2,
            GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height - _texture.Height / 2);
            Bounds = new Rectangle((int)_position.X - _texture.Width/2, (int)_position.Y - _texture.Height / 2, _texture.Width, _texture.Height);
            Origin = new Vector2(_texture.Width / 2, _texture.Height / 2);
        }

        public static CCharachter getInstance(Texture2D texture)
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new CCharachter(texture);
                }
            }
            return instance;
        }

        public void Update()
        {
            offScreen();
            PrevPos = _position;
            Move();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_texture, _position, null, Color.White, _rotation, Origin, 1, SpriteEffects.None, 0f);
        }

        private void Move()
        {
            _bounds.X = (int)Position.X - _texture.Width / 2 + (int)Velocity.X;
            _bounds.Y = (int)Position.Y - _texture.Height / 2 + (int)Velocity.Y;
            Velocity = Vector2.Zero;
            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {
                Velocity.Y = -_speed;
                _rotation = MathHelper.ToRadians(0);
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                Velocity.X = -_speed;
                _rotation = MathHelper.ToRadians(-90);
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                Velocity.Y = _speed;
                _rotation = MathHelper.ToRadians(180);
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                Velocity.X = _speed;
                _rotation = MathHelper.ToRadians(90);
            }
            Position += Velocity;
        }

        private void offScreen()
        {
            if (_position.X < _texture.Width / 2)
            {
                _position.X = _texture.Width / 2;
                Velocity = Vector2.Zero;
            }
            if (_position.Y < _texture.Height / 2)
            {
                _position.Y = _texture.Height / 2;
                Velocity = Vector2.Zero;
            }
            if (_position.X > GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width - (_texture.Width / 2))
            {
                _position.X = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width - (_texture.Width / 2);
                Velocity = Vector2.Zero;
            }
            if (_position.Y > GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height - (_texture.Height / 2))
            { 
            _position.Y = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height - (_texture.Height / 2);
                Velocity = Vector2.Zero;
            }
        }

        public void checkCollision(List<CBasicCharachter> charachters)
        {
            for (int i = 0; i < charachters.Count; i++)
            {
                if(Velocity.X > 0 && isTouchingLeft(charachters[i]) ||
                    Velocity.X < 0 && isTouchingRight(charachters[i]))
                {
                    _position.X = PrevPos.X;
                    _position.Y = PrevPos.Y;
                }
                if (Velocity.Y > 0 && isTouchingTop(charachters[i]) ||
                    Velocity.Y < 0 && isTouchingBottom(charachters[i]))
                {
                    _position.X = PrevPos.X;
                    _position.Y = PrevPos.Y;
                }

            }
        }

        private bool isTouchingLeft(CBasicCharachter oneChar)
        {
            return _bounds.Right + Velocity.X > oneChar.Bounds.Left &&
                _bounds.Left < oneChar.Bounds.Left &&
                _bounds.Bottom > oneChar.Bounds.Top &&
                _bounds.Top < oneChar.Bounds.Bottom;
        }

        private bool isTouchingRight(CBasicCharachter oneChar)
        {
            return _bounds.Left + Velocity.X < oneChar.Bounds.Right &&
                _bounds.Right > oneChar.Bounds.Right &&
                _bounds.Bottom > oneChar.Bounds.Top &&
                _bounds.Top < oneChar.Bounds.Bottom;
        }

        private bool isTouchingTop(CBasicCharachter oneChar)
        {
            return _bounds.Bottom + Velocity.Y > oneChar.Bounds.Top &&
                _bounds.Top < oneChar.Bounds.Top &&
                _bounds.Right > oneChar.Bounds.Left &&
                _bounds.Left < oneChar.Bounds.Right;
        }

        private bool isTouchingBottom(CBasicCharachter oneChar)
        {
            return _bounds.Top + Velocity.Y < oneChar.Bounds.Bottom &&
                _bounds.Bottom > oneChar.Bounds.Bottom &&
                _bounds.Right > oneChar.Bounds.Left &&
                _bounds.Left < oneChar.Bounds.Right;
        }

    }
}
