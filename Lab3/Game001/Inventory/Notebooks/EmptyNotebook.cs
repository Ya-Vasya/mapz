﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    class EmptyNotebook : BaseNotebook
    {

        public EmptyNotebook(string Text, Texture2D texture, Vector2 screenPos, string belongs) : base(Text, texture, screenPos, belongs)
        {
            InnerText = "Blah blaah... Some boring words";
        }

        public override BaseNotebook Clone()
        {
            return new EmptyNotebook(InnerText, Texture, screenPosition, BelongsTo);
        }
    }
}
