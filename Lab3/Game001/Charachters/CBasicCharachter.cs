﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    public abstract class CBasicCharachter
    {
        public string Name { get; private set; }
        protected Texture2D _texture;
        private Vector2 _position;
        public Vector2 Position { get { return _position; } set { _position = value; } }
        public Rectangle Bounds { get { return _bounds; } private set { _bounds = value; } }
        private Rectangle _bounds;

        public CBasicCharachter(Texture2D texture, string name, Random rand)
        {
            _texture = texture;
            setRandomPos(rand);
            Bounds = new Rectangle((int)_position.X, (int)_position.Y, _texture.Width, _texture.Height);
            this.Name = name;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_texture, _position, null, Color.White);
        }

        virtual public void Update()
        {
            
        }

        public void setRandomPos(Random rand)
        { 
            _position = new Vector2(rand.Next(0, 1920), rand.Next(0, 1080));
        }
    }
}
