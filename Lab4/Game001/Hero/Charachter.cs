﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Game001
{
    public class CCharachter //Singleton, Command, Memento
    {

        #region Fields
        private static CCharachter instance;
        private Vector2 _origin;
        public Texture2D _texture;
        private Vector2 _position;
        private Rectangle _bounds;
        private float _rotation = 0;
        public float _speed = 5f;
        private Vector2 _prevPos;
        private Vector2 _velocity;
        private static object syncRoot = new Object();
        Command command; //pattern Command
        #endregion

        #region Properties
        public Vector2 Position { get { return _position; } set { _position = value; } }
        public Rectangle Bounds { get { return _bounds; } private set { _bounds = value; } }
        public string Name { get; private set; }
        public Vector2 PrevPos { get { return _prevPos; } set { _prevPos = value; } }
        #endregion
        
        //Singleton
        #region Main Methods

        private CCharachter(Texture2D texture)
        {
            _texture = texture;
            Position = new Vector2(GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width / 2,
            GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height - _texture.Height / 2);
            Bounds = new Rectangle((int)_position.X - _texture.Width / 2, (int)_position.Y - _texture.Height / 2, _texture.Width, _texture.Height);
            _origin = new Vector2(_texture.Width / 2, _texture.Height / 2);
        }

        public static CCharachter getInstance(Texture2D texture)
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new CCharachter(texture);
                }
            }
            return instance;
        }

        public void Update()
        {
            offScreen();
            PrevPos = _position;
            Move();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_texture, _position, null, Color.White, _rotation, _origin, 1, SpriteEffects.None, 0f);
        }
        #endregion

        #region Additional Methods

        private void Move()
        {
            _bounds.X = (int)Position.X - _texture.Width / 2 + (int)_velocity.X;
            _bounds.Y = (int)Position.Y - _texture.Height / 2 + (int)_velocity.Y;
            _velocity = Vector2.Zero;
            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {
                _velocity.Y = -_speed;
                _rotation = MathHelper.ToRadians(0);
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                _velocity.X = -_speed;
                _rotation = MathHelper.ToRadians(-90);
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                _velocity.Y = _speed;
                _rotation = MathHelper.ToRadians(180);
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                _velocity.X = _speed;
                _rotation = MathHelper.ToRadians(90);
            }
            Position += _velocity;
        }

        private void offScreen()
        {
            if (_position.X < _texture.Width / 2)
            {
                _position.X = _texture.Width / 2;
                _velocity = Vector2.Zero;
            }
            if (_position.Y < _texture.Height / 2)
            {
                _position.Y = _texture.Height / 2;
                _velocity = Vector2.Zero;
            }
            if (_position.X > GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width - (_texture.Width / 2))
            {
                _position.X = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width - (_texture.Width / 2);
                _velocity = Vector2.Zero;
            }
            if (_position.Y > GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height - (_texture.Height / 2))
            { 
            _position.Y = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height - (_texture.Height / 2);
                _velocity = Vector2.Zero;
            }
        }

        public void checkCollision(List<CBasicCharachter> charachters)
        {
            for (int i = 0; i < charachters.Count; i++)
            {
                if(_velocity.X > 0 && isTouchingLeft(charachters[i]) ||
                    _velocity.X < 0 && isTouchingRight(charachters[i]))
                {
                    _position.X = PrevPos.X;
                    _position.Y = PrevPos.Y;
                }
                if (_velocity.Y > 0 && isTouchingTop(charachters[i]) ||
                    _velocity.Y < 0 && isTouchingBottom(charachters[i]))
                {
                    _position.X = PrevPos.X;
                    _position.Y = PrevPos.Y;
                }

            }
        }


        private bool isTouchingLeft(CBasicCharachter oneChar)
        {
            return _bounds.Right + _velocity.X > oneChar.Bounds.Left &&
                _bounds.Left < oneChar.Bounds.Left &&
                _bounds.Bottom > oneChar.Bounds.Top &&
                _bounds.Top < oneChar.Bounds.Bottom;
        }

        private bool isTouchingRight(CBasicCharachter oneChar)
        {
            return _bounds.Left + _velocity.X < oneChar.Bounds.Right &&
                _bounds.Right > oneChar.Bounds.Right &&
                _bounds.Bottom > oneChar.Bounds.Top &&
                _bounds.Top < oneChar.Bounds.Bottom;
        }

        private bool isTouchingTop(CBasicCharachter oneChar)
        {
            return _bounds.Bottom + _velocity.Y > oneChar.Bounds.Top &&
                _bounds.Top < oneChar.Bounds.Top &&
                _bounds.Right > oneChar.Bounds.Left &&
                _bounds.Left < oneChar.Bounds.Right;
        }

        private bool isTouchingBottom(CBasicCharachter oneChar)
        {
            return _bounds.Top + _velocity.Y < oneChar.Bounds.Bottom &&
                _bounds.Bottom > oneChar.Bounds.Bottom &&
                _bounds.Right > oneChar.Bounds.Left &&
                _bounds.Left < oneChar.Bounds.Right;
        }
        #endregion

        //For pattern Command
        #region Command
        public void Push()
        {
            command.Execute();
        }

        public void SetCommand(Command comm)
        {
            command = comm;
        }
        #endregion

        //For pattern Memento
        #region Memento

        public HeroMemento SaveState()
        {
            return new HeroMemento(Position, Bounds);
        }

        public void RestoreState(HeroMemento memento)
        {
            this.Position = memento.Position;
            this.Bounds = memento.Bounds;
        }

        #endregion
    }
}
