﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    public class GameHistory //Memento
    {

        public HeroMemento LastState { get; set; }

        public GameHistory()
        {
            LastState = new HeroMemento(new Microsoft.Xna.Framework.Vector2(), new Microsoft.Xna.Framework.Rectangle());
        }

    }
}
