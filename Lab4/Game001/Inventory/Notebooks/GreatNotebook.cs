﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    class GreatNotebook : BaseNotebook
    {

        public GreatNotebook(string Text, Texture2D texture, Vector2 screenPos, string belongs) : base(Text, texture, screenPos, belongs)
        {
            ///to do read data from file
        }

        public override BaseNotebook Clone()
        {
           BaseNotebook instance = new GreatNotebook(InnerText, Texture, screenPosition, BelongsTo);
            instance.setRandomPos();
            return instance;
        }

        public override void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            base.Draw(spriteBatch, font);
            spriteBatch.DrawString(font, "GreatNotebook", new Vector2(screenPosition.X, screenPosition.Y - 45), Color.White);
        }
    }
}
