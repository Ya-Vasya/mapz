﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    class InteractionManager : IObservable //Observer
    {
        #region Fields
        private CCharachter _main;
        private List<CBasicCharachter> _charachters;
        private Command _command;
        #endregion

        #region Properties
        public CCharachter Main { get { return _main; } set { _main = value; } }
        public List<CBasicCharachter> Charachters { get { return _charachters; } set { _charachters = value; } }
        #endregion

        #region Main Methods
        public InteractionManager(CCharachter hero, List<CBasicCharachter> charachters, Command comm)
        {
            Main = hero;
            Charachters = charachters;
            _command = comm;
            Main.SetCommand(_command);
        }

        public void Update()
        {
            int dist = 0;
            foreach (var mate in Charachters)
            {
                dist = checkIfClose(mate);
                if(dist != -1)
                {
                    _command.setNewCharachter(mate);
                    Main.SetCommand(_command);
                }
                else
                {
                    _command.SetToNull();
                    Main.SetCommand(null);
                }
            }

            if (Keyboard.GetState().IsKeyDown(Keys.E) && _command.interactableCharachter != null)
            {
                Main.Push();
            }

            NotifyObservers();

        }

        public int checkIfClose(CBasicCharachter mate)
        {

            double distance = 0;

                distance = Math.Sqrt(Math.Pow((mate.Position.X - Main.Position.X),2)+Math.Pow((mate.Position.Y - Main.Position.Y),2));

                if(distance < 150)
                {
                    return Charachters.FindIndex(x => x == mate);
                }
            return -1;
        }
        #endregion

        #region Observer

        public void AddObserver(CBasicCharachter o)
        {
            Charachters.Add(o);
        }

        public void RemoveObserver(CBasicCharachter o)
        {
            Charachters.Remove(o);
        }

        public void NotifyObservers()
        {
            foreach (var mate in Charachters)
            {
                mate.UpdateHeroPosition(Main.Position);
            }
        }
        #endregion

    }
}
