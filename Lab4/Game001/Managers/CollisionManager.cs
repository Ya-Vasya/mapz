﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    class CollisionManager //Facade
    {
        #region Fields
        private CCharachter _main;
        private List<CBasicCharachter> _charachters;
        private List<IItem> _itemsList;
        #endregion

        #region Properties
        public CCharachter Main { get { return _main; } set { _main = value; } }
        public List<CBasicCharachter> Charachters { get { return _charachters; } set { _charachters = value; } }
        public List<IItem> ItemsList { get { return _itemsList; } set { _itemsList = value; } }
        #endregion

        #region Methods

        public CollisionManager()
        {

        }

        public void pickUp()
        {
            for(int i = 0; i < ItemsList.Count; i++)
            {
                if(ItemsList[i].Intersects(Main))
                {
                    ItemsList.RemoveAt(i);
                }
            }
        }

        public void Colide()
        {
            _main.checkCollision(_charachters);
        }

            
        public void Update()
        {
            pickUp();
            Colide();
        }
        #endregion
    }
}
