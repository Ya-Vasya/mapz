﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    public interface IObservable //Observer
    {

        void AddObserver(CBasicCharachter o);
        void RemoveObserver(CBasicCharachter o);
        void NotifyObservers();

    }
}
